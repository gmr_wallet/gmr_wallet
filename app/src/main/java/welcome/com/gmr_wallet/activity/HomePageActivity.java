package welcome.com.gmr_wallet.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import welcome.com.gmr_wallet.R;

/**
 * Created by home on 8/19/2016.
 */
public class HomePageActivity extends AppCompatActivity {

    Button overview;
    Button aboutus;
    Button budget;
    Button income;
    Button expense;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_page);

        addListenerOnButton();

    }

    private void addListenerOnButton() {
        overview = (Button) findViewById(R.id.overview);
        aboutus = (Button) findViewById(R.id.aboutus);
        budget = (Button) findViewById(R.id.budget);
        income = (Button) findViewById(R.id.incomebutton);
        expense = (Button) findViewById(R.id.expensebutton);


        budget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomePageActivity.this, BudgetListActivity.class);
                startActivity(intent);
            }
        });
        income.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomePageActivity.this, IncomeListActivity.class);
                startActivity(intent);
            }
        });

        expense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomePageActivity.this, ExpenseListActivity.class);
                startActivity(intent);
            }
        });
        aboutus.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View v) {
                                           Intent intent = new Intent(HomePageActivity.this, AboutusActivity.class);
                                           startActivity(intent);
                                       }
                                   }
        );

        overview.setOnClickListener(new View.OnClickListener()

                                    {
                                        @Override
                                        public void onClick(View v) {
                                            Intent intent = new Intent(HomePageActivity.this, OverviewActivity.class);
                                            startActivity(intent);
                                        }
                                    }
        );

    }

    @Override
    public void onBackPressed() {
        Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(setIntent);
    }

}
