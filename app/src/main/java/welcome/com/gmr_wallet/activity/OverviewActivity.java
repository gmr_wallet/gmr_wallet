package welcome.com.gmr_wallet.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import welcome.com.gmr_wallet.R;
import welcome.com.gmr_wallet.db.DataBaseHandler;
import welcome.com.gmr_wallet.domain.ExpenseListData;
import welcome.com.gmr_wallet.domain.IncomeListData;

/**
 * Created by home on 8/19/2016.
 */
public class OverviewActivity extends CommonActivity {
    public TextView netIncome_tv, totalExpense_tv, totalIncome_tv;
    long totalExpense = 0;
    long totalIncome = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.overview);

        netIncome_tv = (TextView) findViewById(R.id.rupesNet_tv);
        totalExpense_tv = (TextView) findViewById(R.id.rupesExpense_tv);
        totalIncome_tv = (TextView) findViewById(R.id.totalIncomeRupes_tv);

        getTotalIncome();
        getTotalExpense();
        getNetIncome();

    }

    private void getTotalIncome() {
        DataBaseHandler db = new DataBaseHandler(this);
        List<IncomeListData> incomes = db.getAllIncome();
        List<Integer> totalIncomeList = new ArrayList<Integer>();
        if (!incomes.isEmpty() && incomes.size() > 0) {
            for (IncomeListData income : incomes) {
                String incomesAmount = income.getIncomeAmount();
                totalIncomeList.add(Integer.parseInt(incomesAmount));
            }
        }
        if (totalIncomeList.size() > 0) {
            for (int income : totalIncomeList) {
                totalIncome = totalIncome + income;
            }
            totalIncome_tv.setText(" " + totalIncome);
        }
    }


    private int getTotalExpense() {
        DataBaseHandler db = new DataBaseHandler(this);
        List<ExpenseListData> expenses = db.getAllExpense();
        List<Integer> totalExpenseList = new ArrayList<Integer>();
        if (!expenses.isEmpty() && expenses.size() > 0) {
            for (ExpenseListData expense : expenses) {
                String expenseAmount = expense.getExpenseAmount();
                totalExpenseList.add(Integer.parseInt(expenseAmount));

            }
        }
        if (totalExpenseList.size() > 0) {
            for (int expense : totalExpenseList) {
                totalExpense = totalExpense + expense;
            }
            totalExpense_tv.setText(" " + totalExpense);
        }
        return 0;
    }

    private void getNetIncome() {
        long netIncome = totalIncome - totalExpense;
        netIncome_tv.setText("" + netIncome);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onBackPressed() {
        Intent intent = new Intent(OverviewActivity.this, HomePageActivity.class);
        startActivity(intent);
    }

}

