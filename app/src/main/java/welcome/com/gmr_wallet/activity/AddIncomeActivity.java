package welcome.com.gmr_wallet.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import welcome.com.gmr_wallet.R;
import welcome.com.gmr_wallet.db.DataBaseHandler;


public class
AddIncomeActivity extends CommonActivity implements AdapterView.OnItemSelectedListener {
    EditText commentIncome, amountIncome;
    Button saveIncome;
    String[] transactionIncome = {"Select", "Education", "Phone Bill", "Insurance", "Housing", "Food", "Utilities", "Retail",
            "Automotive", "Transportation", "TV/Cable", "Other"};
    private String incomeId;
    private String incomeCategory;

    //date and time
    Calendar calander;
    SimpleDateFormat simpledateformat;
    String incomeDT;
    TextView incomeDateTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addincome);

        final Spinner spinner_transactionIncome = (Spinner) findViewById(R.id.spinner_itransaction);
        spinner_transactionIncome.setOnItemSelectedListener(this);
        ArrayAdapter ia = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, transactionIncome);
        spinner_transactionIncome.setAdapter(ia);


        incomeDateTime = (TextView)findViewById(R.id.incomeDT);
        commentIncome = (EditText) findViewById(R.id.commentIncome);
        amountIncome = (EditText) findViewById(R.id.amountIncome);
        saveIncome = (Button) findViewById(R.id.incomesave);

        // get values from adopter
        final Intent intent = getIntent();
        incomeId = intent.getStringExtra("eId");
        String category = intent.getStringExtra("category");
        final String comment = intent.getStringExtra("comment");
        final String amount = intent.getStringExtra("Amount");
        final String dateTime = intent.getStringExtra("Date and Time");

        // set values to edit texts
        amountIncome.setText(amount);
        commentIncome.setText(comment);
        incomeDateTime.setText(dateTime);

        // set value to spinner
        setSpinnerSelection(spinner_transactionIncome, transactionIncome, category);
        spinner_transactionIncome.setOnItemSelectedListener(this);

        //date and time
        calander = Calendar.getInstance();
//        simpledateformat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
        simpledateformat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        incomeDT = simpledateformat.format(calander.getTime());
        incomeDateTime.setText(incomeDT);

        saveIncome.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                String cmntIncome = commentIncome.getText().toString();
                String iAmount = amountIncome.getText().toString();

                if (spinner_transactionIncome.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    Toast.makeText(getApplicationContext(), "Please select category", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    if (intent.hasExtra("Amount") && intent.hasExtra("comment")) { // update
                        // update code goes here..
                        incomeCategory = spinner_transactionIncome.getSelectedItem().toString();
                        DataBaseHandler db = new DataBaseHandler(getApplicationContext());
                        db.updateIncome(incomeId, cmntIncome, iAmount, incomeCategory,incomeDT);
                        Toast.makeText(getApplicationContext(), "updated successfully...", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(), IncomeListActivity.class));

                    } else if (cmntIncome.trim().length() > 0 && iAmount.trim().length() > 0) { // save
                        incomeCategory = spinner_transactionIncome.getSelectedItem().toString();
                        DataBaseHandler db = new DataBaseHandler(getApplicationContext());
                        db.insertIncomeInformation(cmntIncome, iAmount, incomeCategory,incomeDT);
                        Toast.makeText(getApplicationContext(), " Details Saved Successfully", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(AddIncomeActivity.this, IncomeListActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), "Please enter mandatory fields..", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    public void setSpinnerSelection(Spinner spinner,String[] array,String text) {
        for(int i=0;i<array.length;i++) {
            if(array[i].equals(text)) {
                spinner.setSelection(i);
            }
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

}



