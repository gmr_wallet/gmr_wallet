package welcome.com.gmr_wallet.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import welcome.com.gmr_wallet.R;
import welcome.com.gmr_wallet.adapter.MyCustomAdapterBudget;
import welcome.com.gmr_wallet.db.DataBaseHandler;
import welcome.com.gmr_wallet.domain.BudgetListData;


public class BudgetListActivity extends CommonActivity implements AdapterView.OnItemClickListener {

    FloatingActionButton fab1;
    MyCustomAdapterBudget mAdapter;
    ArrayList myList = new ArrayList();
    ListView lvDetails;
    private List<BudgetListData> budgets;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.budgetlist);

        lvDetails = (ListView) findViewById(android.R.id.list);
        addListenerOnButton();
        budgets = getBudgets();

        /*count*/
//        View viewById = findViewById(R.id.budgetlistcontentID);
//        TextView count_tvNo = (TextView) viewById.findViewById(R.id.count_tvno);
//        count_tvNo.setText("" + budgets.size());

        getDataInList();

        lvDetails.setAdapter(new MyCustomAdapterBudget(this, myList));

    }

    private void addListenerOnButton() {
        fab1 = (FloatingActionButton) findViewById(R.id.bFab);
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BudgetListActivity.this, AddBudgetActivity.class);
                startActivity(intent);
            }
        });
    }

    private void getDataInList() {
        if (budgets != null && !budgets.isEmpty()) {

            for (BudgetListData budget : budgets) {
                BudgetListData ld = new BudgetListData();
                ld.setBudgeDT(budget.getBudgeDT());
                ld.setBudgetId(budget.getBudgetId());
                ld.setBudgetCategory(budget.getBudgetCategory());
                ld.setBudgetInterval(budget.getBudgetInterval());
                ld.setBudgetName(budget.getBudgetName());
                ld.setBudgetAmount(budget.getBudgetAmount());

                myList.add(ld);
            }
        }
    }


    private void setBudgetsToCustomAdopter(List<String> budgets) {
        if (budgets != null && !budgets.isEmpty()) {
            mAdapter.setBudgets(budgets);
        }
    }

    private List<BudgetListData> getBudgets() {
        DataBaseHandler db = new DataBaseHandler(this);
        List<BudgetListData> budgets = db.getAllBudget();
        return budgets;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    public void onBackPressed() {
        Intent intent = new Intent(BudgetListActivity.this, HomePageActivity.class);
        startActivity(intent);
    }

}



