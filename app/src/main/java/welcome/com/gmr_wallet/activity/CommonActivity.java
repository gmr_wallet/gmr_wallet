package welcome.com.gmr_wallet.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import welcome.com.gmr_wallet.R;

/**
 * Created by Shylendra on 07-Nov-16.
 */
public class CommonActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // back to Home
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.overView:
                startActivity(new Intent(CommonActivity.this, OverviewActivity.class));
                return true;
            case R.id.budget:
                startActivity(new Intent(CommonActivity.this, BudgetListActivity.class));
                return true;
            case R.id.income:
                startActivity(new Intent(CommonActivity.this, IncomeListActivity.class));
                return true;
            case R.id.expense:
                startActivity(new Intent(CommonActivity.this, ExpenseListActivity.class));
                return true;
            case R.id.aboutUs:
                startActivity(new Intent(CommonActivity.this, AboutusActivity.class));
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
