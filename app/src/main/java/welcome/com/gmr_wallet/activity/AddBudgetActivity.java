package welcome.com.gmr_wallet.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import welcome.com.gmr_wallet.R;
import welcome.com.gmr_wallet.db.DataBaseHandler;


/**
 * Created by raj on 8/19/2016.
 */
public class AddBudgetActivity extends CommonActivity implements AdapterView.OnItemSelectedListener {
    EditText bName, bAmount;
    Button saveButton;

    String[] budgetTransaction = {"Select", "Education", "Phone Bill", "Insurance", "Housing", "Food", "Utilities", "Retail",
            "Automotive", "Transportation", "TV/Cable", "Other"};
    String[] budgetIntervalTransaction = {"Select", "Weekly", "Every Two weeks", "Every Four weeks", "Monthly",
            "Every Two Months", "Every Three Months", "Every Six Months", "Every Year", "Other"};

    private String budgetId;
    private String budgetCategory;
    private String budgetInterval;
    //date and time

    Calendar calander;
    SimpleDateFormat simpledateformat;
    String budgetDT;
    TextView budgetDateTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addbudget);

        final Spinner spinner_transaction = (Spinner) findViewById(R.id.cateIncome_spinner);
        spinner_transaction.setOnItemSelectedListener(this);
        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, budgetTransaction);
        spinner_transaction.setAdapter(aa);

        final Spinner spinner_transaction1 = (Spinner) findViewById(R.id.spinner_transactionBudget);
        spinner_transaction1.setOnItemSelectedListener(this);
        ArrayAdapter aa1 = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, budgetIntervalTransaction);
        spinner_transaction1.setAdapter(aa1);

        budgetDateTime = (TextView)findViewById(R.id.budgetDT);
        bName = (EditText) findViewById(R.id.BudgetNameET);
        bAmount = (EditText) findViewById(R.id.bAmountET);

        saveButton = (Button) findViewById(R.id.buttonSave);
        final Intent intent = getIntent();
        budgetId = intent.getStringExtra("budgetId");
        String bCategory = intent.getStringExtra("budgetCategory");
        String bInterval = intent.getStringExtra("budgetInterval");
        final String budgetName = intent.getStringExtra("budgetName");
        final String budgetAmount = intent.getStringExtra("budgetAmount");
        final String dateTime = intent.getStringExtra("Date and Time");

        //set values to editTexts
        bName.setText(budgetName);
        bAmount.setText(budgetAmount);
        budgetDateTime.setText(dateTime);
        //set values to Spinners
        setSpinnerSelection(spinner_transaction, budgetTransaction, bCategory);
        setSpinnerSelection(spinner_transaction1, budgetIntervalTransaction, bInterval);

        spinner_transaction.setOnItemSelectedListener(this);
        spinner_transaction1.setOnItemSelectedListener(this);
        //date and time
        calander = Calendar.getInstance();
//        simpledateformat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
        simpledateformat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        budgetDT = simpledateformat.format(calander.getTime());
        budgetDateTime.setText(budgetDT);

        saveButton.setOnClickListener((new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                String budgetName = bName.getText().toString();
                String budgetAmount = bAmount.getText().toString();

                if (spinner_transaction.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    Toast.makeText(getApplicationContext(), "Please select category", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (spinner_transaction1.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    Toast.makeText(getApplicationContext(), "Please select budget interval", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    if (intent.hasExtra("budgetName") && intent.hasExtra("budgetAmount")) {
                        // update code goes here..
                        budgetCategory = spinner_transaction.getSelectedItem().toString();
                        budgetInterval = spinner_transaction1.getSelectedItem().toString();
                        DataBaseHandler db = new DataBaseHandler(getApplicationContext());
                        db.updateBudget(budgetId, budgetName, budgetAmount, budgetCategory, budgetInterval,budgetDT);
                        Toast.makeText(getApplicationContext(), "updated successfully...", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(), BudgetListActivity.class));

                    } else if (budgetName.trim().length() > 0 && budgetAmount.trim().length() > 0) { //save
                        budgetCategory = spinner_transaction.getSelectedItem().toString();
                        budgetInterval = spinner_transaction1.getSelectedItem().toString();
                        DataBaseHandler db = new DataBaseHandler(getApplicationContext());
                        db.insertBudgetInformation(budgetName, budgetAmount, budgetCategory, budgetInterval,budgetDT);
                        Toast.makeText(getApplicationContext(), " Details Saved Successfully", Toast.LENGTH_SHORT).show();
                        bName.setText("");
                        bAmount.setText("");
                        Intent intent = new Intent(AddBudgetActivity.this, BudgetListActivity.class);
                        startActivity(intent);
                        // to hide keypad
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(bName.getWindowToken(), 0);
                        imm.hideSoftInputFromWindow(bAmount.getWindowToken(), 0);
                    } else {
                        Toast.makeText(getApplicationContext(), "please enter Budget name and amount", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
        ));
    }

    public void setSpinnerSelection(Spinner spinner,String[] array,String text) {
        for(int i=0;i<array.length;i++) {
            if(array[i].equals(text)) {
                spinner.setSelection(i);
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}

