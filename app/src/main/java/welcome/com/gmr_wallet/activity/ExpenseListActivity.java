package welcome.com.gmr_wallet.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import welcome.com.gmr_wallet.R;
import welcome.com.gmr_wallet.adapter.MyCustomAdapterExpense;
import welcome.com.gmr_wallet.db.DataBaseHandler;
import welcome.com.gmr_wallet.domain.ExpenseListData;

/**
 * Created by home on 8/27/2016.
 */
public class ExpenseListActivity extends CommonActivity implements AdapterView.OnItemClickListener {

    FloatingActionButton fab3;
    MyCustomAdapterExpense myExpenseAdapter;
    ArrayList myListExpense = new ArrayList();
    ListView listExpenseDetails;

    private List<ExpenseListData> expenses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.expenselist);

        listExpenseDetails = (ListView) findViewById(android.R.id.list);
        registerForContextMenu(listExpenseDetails);

        addListenerOnButton();

        expenses = getExpenses();
        getDataExpenseList();

        listExpenseDetails.setAdapter(new MyCustomAdapterExpense(this, myListExpense));
    }

    private void getDataExpenseList() {
        if (expenses != null && !expenses.isEmpty()) {

            for (ExpenseListData expense : expenses) {

                ExpenseListData ld = new ExpenseListData();
                ld.setExpenseDT(expense.getExpenseDT());
                ld.setExpenseCategory(expense.getExpenseCategory());
                ld.setExpenseComment(expense.getExpenseComment());
                ld.setExpenseAmount(expense.getExpenseAmount());
                ld.setID(expense.getID());


                // Add this object into the ArrayList myList
                myListExpense.add(ld);
            }
        }

    }

    private void setExpensesToCustomAdopter(List<String> expenses) {
        if (expenses != null && !expenses.isEmpty()) {
            myExpenseAdapter.setExpenses(expenses);
        }
    }

    private List<ExpenseListData> getExpenses() {
        DataBaseHandler db = new DataBaseHandler(this);
        List<ExpenseListData> expenses = db.getAllExpense();
        return expenses;
    }


    private void addListenerOnButton() {
        fab3 = (FloatingActionButton) findViewById(R.id.eFab);

        fab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ExpenseListActivity.this, AddExpenseActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


    }

    public void onBackPressed() {
        Intent intent = new Intent(ExpenseListActivity.this, HomePageActivity.class);
        startActivity(intent);
    }

 }