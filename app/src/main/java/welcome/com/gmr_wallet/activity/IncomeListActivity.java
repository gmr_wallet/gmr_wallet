package welcome.com.gmr_wallet.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import welcome.com.gmr_wallet.R;

import welcome.com.gmr_wallet.adapter.MyCustomAdapterIncome;

import welcome.com.gmr_wallet.db.DataBaseHandler;
import welcome.com.gmr_wallet.domain.IncomeListData;

/**
 * Created by home on 8/27/2016.
 */
public class IncomeListActivity extends CommonActivity implements AdapterView.OnItemClickListener {

    FloatingActionButton fab3;
    MyCustomAdapterIncome myIncomeAdapter;
    ArrayList myListIncome = new ArrayList();
    ListView listIncomeDetails;

    private List<IncomeListData> incomes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.incomelist);
        // back to Home
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listIncomeDetails = (ListView) findViewById(android.R.id.list);
        registerForContextMenu(listIncomeDetails);

        addListenerOnButton();

        incomes = getIncomes();
        getDataIncomeList();

        listIncomeDetails.setAdapter(new MyCustomAdapterIncome(this, myListIncome));

    }

    private void getDataIncomeList() {
        if (incomes != null && !incomes.isEmpty()) {

            for (IncomeListData income : incomes) {

                IncomeListData ld = new IncomeListData();
                ld.setIncomeDT(income.getIncomeDT());
                ld.setIncomeCategory(income.getIncomeCategory());
                ld.setIncomeComment(income.getIncomeComment());
                ld.setIncomeAmount(income.getIncomeAmount());
                ld.setID(income.getID());


// Add this object into the ArrayList myList
                myListIncome.add(ld);
            }
        }

    }

    private void setIncomesToCustomAdopter(List<String> incomes) {
        if (incomes != null && !incomes.isEmpty()) {
            myIncomeAdapter.setIncomes(incomes);
        }
    }

    private List<IncomeListData> getIncomes() {
        DataBaseHandler db = new DataBaseHandler(this);
        List<IncomeListData> incomes = db.getAllIncome();
        return incomes;
    }


    private void addListenerOnButton() {
        fab3 = (FloatingActionButton) findViewById(R.id.iFab);

        fab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(IncomeListActivity.this, AddIncomeActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    }

    public void onBackPressed() {
        Intent intent = new Intent(IncomeListActivity.this, HomePageActivity.class);
        startActivity(intent);
    }

}