package welcome.com.gmr_wallet.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import welcome.com.gmr_wallet.R;
import welcome.com.gmr_wallet.db.DataBaseHandler;


public class AddExpenseActivity extends CommonActivity implements AdapterView.OnItemSelectedListener {
    EditText commentExpense, amountExpense;
    Button saveExpense;
    String[] transactionExpense = {"Select", "Education", "Phone Bill", "Insurance", "Housing", "Food", "Utilities", "Retail",
            "Automotive", "Transportation", "TV/Cable", "Other"};
    private String expenseId;
    private String expenseCategory;

    //date and time
    Calendar calander;
    SimpleDateFormat simpledateformat;
    String expenseDT;
    TextView expenseDateTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addexpense);

        final Spinner spinner_transactionExpense = (Spinner) findViewById(R.id.spinner_transaction);
        spinner_transactionExpense.setOnItemSelectedListener(this);
        ArrayAdapter ia = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, transactionExpense);
        spinner_transactionExpense.setAdapter(ia);

        expenseDateTime = (TextView) findViewById(R.id.expenseDT);
        commentExpense = (EditText) findViewById(R.id.commentExpense);
        amountExpense = (EditText) findViewById(R.id.amountExpense);
        saveExpense = (Button) findViewById(R.id.saveExpense);

        // get values from adopter
        final Intent intent = getIntent();
        expenseId = intent.getStringExtra("eId");
        String category = intent.getStringExtra("category");
        final String comment = intent.getStringExtra("comment");
        final String amount = intent.getStringExtra("Amount");
        final String dateTime = intent.getStringExtra("Date and Time");

        // set values to edit texts
        amountExpense.setText(amount);
        commentExpense.setText(comment);
        expenseDateTime.setText(dateTime);

        // set value to spinner
        setSpinnerSelection(spinner_transactionExpense, transactionExpense, category);
        spinner_transactionExpense.setOnItemSelectedListener(this);

        //date and time
        calander = Calendar.getInstance();
//        simpledateformat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
        simpledateformat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        expenseDT = simpledateformat.format(calander.getTime());
        expenseDateTime.setText(expenseDT);

        saveExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cmntExpense = commentExpense.getText().toString();
                String eAmount = amountExpense.getText().toString();

                if (spinner_transactionExpense.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    Toast.makeText(getApplicationContext(), "Please select category", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    if (intent.hasExtra("Amount") && intent.hasExtra("comment")) { // update
                        // update code goes here..
                        expenseCategory = spinner_transactionExpense.getSelectedItem().toString();
                        DataBaseHandler db = new DataBaseHandler(getApplicationContext());
                        db.updateExpense(expenseId, cmntExpense, eAmount, expenseCategory, expenseDT);
                        Toast.makeText(getApplicationContext(), "updated successfully...", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(), ExpenseListActivity.class));

                    } else if (cmntExpense.trim().length() > 0 || eAmount.trim().length() > 0) { // save
                        expenseCategory = spinner_transactionExpense.getSelectedItem().toString();
                        DataBaseHandler db = new DataBaseHandler(getApplicationContext());
                        db.insertExpenseInformation(cmntExpense, eAmount, expenseCategory, expenseDT);
                        Toast.makeText(getApplicationContext(), " Details Saved Successfully", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(AddExpenseActivity.this, ExpenseListActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), "Please enter mandatory fields..", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    public void setSpinnerSelection(Spinner spinner, String[] array, String text) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(text)) {
                spinner.setSelection(i);
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}



