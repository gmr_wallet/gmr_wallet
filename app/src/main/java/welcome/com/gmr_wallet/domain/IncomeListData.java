
package welcome.com.gmr_wallet.domain;

/**
 * Created by home on 8/23/2016.
 */
public class IncomeListData {
    int id;
    String incomeDT;
    String incomeCategory;
    String incomeComment;
    String incomeAmount;

    public String getIncomeDT() {
        return incomeDT;
    }

    public void setIncomeDT(String incomeDT) {
        this.incomeDT = incomeDT;
    }

    public String getIncomeCategory() {
        return incomeCategory;
    }

    public void setIncomeCategory(String incomeTransaction) {
        this.incomeCategory = incomeTransaction;
    }

    public String getIncomeComment() {
        return incomeComment;
    }

    public void setIncomeComment(String incomeComment) {
        this.incomeComment = incomeComment;
    }



    public String getIncomeAmount() {
        return incomeAmount;
    }

    public void setIncomeAmount(String incomeAmount) {
        this.incomeAmount = incomeAmount;
    }

    public int getID() {
        return this.id;

    }
    public  void setID(int id){
        this.id  = id;
    }
}
