package welcome.com.gmr_wallet.domain;

/**
 * Created by home on 8/23/2016.
 */
public class BudgetListData {

    int budgetId;
    String budgetAmount;
    String budgetCategory;
    String budgetInterval;
    String budgetName;
    String budgeDT;
    public int getBudgetId() {
        return budgetId;
    }

    public void setBudgetId(int id) {
        this.budgetId = id;
    }




    public String getBudgetAmount() {
        return budgetAmount;
    }

    public void setBudgetAmount(String budgetAmount) {
        this.budgetAmount = budgetAmount;
    }

    public String getBudgetCategory() {
        return budgetCategory;
    }

    public void setBudgetCategory(String budgetCategory) {
        this.budgetCategory = budgetCategory;
    }

    public String getBudgetInterval() {
        return budgetInterval;
    }

    public void setBudgetInterval(String budgetInterval) {
        this.budgetInterval = budgetInterval;
    }

    public String getBudgetName() {
        return budgetName;
    }

    public void setBudgetName(String budgetName) {
        this.budgetName = budgetName;
    }

    public String getBudgeDT() {
        return budgeDT;
    }

    public void setBudgeDT(String budgeDT) {
        this.budgeDT = budgeDT;
    }
}

