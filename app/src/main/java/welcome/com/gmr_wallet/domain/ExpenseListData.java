
package welcome.com.gmr_wallet.domain;

/**
 * Created by home on 8/23/2016.
 */
public class ExpenseListData  {
    int id;
    String expenseDT;
    String expenseCategory;
    String expenseComment;
    String expenseAmount;

    public String getExpenseDT() {
        return expenseDT;
    }

    public void setExpenseDT(String expenseDT) {
        this.expenseDT = expenseDT;
    }

    public String getExpenseCategory() {
        return expenseCategory;
    }

    public void setExpenseCategory(String expenseTransaction) {
        this.expenseCategory = expenseTransaction;
    }

    public String getExpenseComment() {
        return expenseComment;
    }

    public void setExpenseComment(String expenseComment) {
        this.expenseComment = expenseComment;
    }



    public String getExpenseAmount() {
        return expenseAmount;
    }

    public void setExpenseAmount(String expenseAmount) {
        this.expenseAmount = expenseAmount;
    }

    public int getID() {
        return this.id;

    }
    public  void setID(int id){
        this.id  = id;
    }
}
