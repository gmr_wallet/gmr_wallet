package welcome.com.gmr_wallet.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import welcome.com.gmr_wallet.R;

import welcome.com.gmr_wallet.activity.AddExpenseActivity;
import welcome.com.gmr_wallet.activity.ExpenseListActivity;
import welcome.com.gmr_wallet.db.DataBaseHandler;
import welcome.com.gmr_wallet.domain.ExpenseListData;

/**
 * Created by home on 8/29/2016.
 */
public class MyCustomAdapterExpense extends BaseAdapter {

    ArrayList expenseList = new ArrayList();
    LayoutInflater myExpenseInflater;
    Context expenseContext;


    private ExpenseListActivity expenseListActivity;

    public MyCustomAdapterExpense(Context context, ArrayList expenseList) {
        this.expenseList = expenseList;
        this.expenseContext = context;
        myExpenseInflater = LayoutInflater.from(context);

    }


    public void setExpenses(List<String> expenses) {
        if (expenses.size() > 0) {
            this.expenseList.clear();
            this.expenseList = expenseList;
        }
    }

    public void addItem(final String item) {
        expenseList.add(item);

    }


    @Override
    public int getCount() {
        return expenseList.size();
    }

    @Override
    public ExpenseListData getItem(int position) {
        return (ExpenseListData) expenseList.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        MyViewHolder holder = null;
        int type = getItemViewType(position);
        expenseList.get(position);
        if (convertView == null) {

            myExpenseInflater = (LayoutInflater) expenseContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = myExpenseInflater.inflate(R.layout.expenselist_textview, null);

            holder = new MyViewHolder(convertView);
            holder.expenseDT_tv = (TextView)convertView.findViewById(R.id.expenseDT_tv);
            holder.expenseTransaction_tv = (TextView) convertView.findViewById(R.id.expensetransaction_tv);
            holder.expenseComment_tv = (TextView) convertView.findViewById(R.id.expenseComment_tv);
            holder.expenseAmount_tv = (TextView) convertView.findViewById(R.id.expenseAmount_tv);
            holder.expenseImageButton = (ImageButton) convertView.findViewById(R.id.expenseImageButton);


            convertView.setTag(holder);
        } else {
            holder = (MyViewHolder) convertView.getTag();
        }
        ExpenseListData currentExpenseListData = getItem(position);
        holder.expenseDT_tv.setText(currentExpenseListData.getExpenseDT());
        holder.expenseTransaction_tv.setText(currentExpenseListData.getExpenseCategory());
        holder.expenseComment_tv.setText(currentExpenseListData.getExpenseComment());
        holder.expenseAmount_tv.setText(currentExpenseListData.getExpenseAmount());

        holder.expenseImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                final PopupMenu popup = new PopupMenu(expenseContext, v);
                popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {

                        if (item.getTitle().equals("update")) {
                            //TODO updte codegoes here
                            ExpenseListData expenseListData = getItem(position);

                            Intent i = new Intent(expenseContext, AddExpenseActivity.class);
                            i.putExtra("eId", expenseListData.getID() + "");
                            i.putExtra("Date and Time",expenseListData.getExpenseDT());
                            i.putExtra("category", expenseListData.getExpenseCategory());
                            i.putExtra("comment", expenseListData.getExpenseComment());
                            i.putExtra("Amount", expenseListData.getExpenseAmount());

                            expenseContext.startActivity(i);

                        }
                        if (item.getTitle().equals("delete")) {
                            //delete code
                            ExpenseListData item1 = getItem(position);
                            DataBaseHandler dataBaseHandler = new DataBaseHandler(expenseContext);
                            dataBaseHandler.deleteExpense(item1.getID());

                            Toast.makeText(expenseContext, "Expense deleted successfully...", Toast.LENGTH_SHORT).show();
                            expenseList.clear();
                            List<ExpenseListData> expense = dataBaseHandler.getAllExpense();
                            expenseList.addAll(expense);
                            notifyDataSetChanged();
                        }

                        return true;
                    }
                });

                popup.show();
            }


        });


        return convertView;
    }

    private class MyViewHolder {
        TextView expenseDT_tv;
        TextView expenseTransaction_tv;
        TextView expenseComment_tv;
        TextView expenseAmount_tv;
        ImageButton expenseImageButton;

        public MyViewHolder(View convertView) {
            expenseDT_tv = (TextView) convertView.findViewById(R.id.expenseDT_tv);
            expenseTransaction_tv = (TextView) convertView.findViewById(R.id.expensetransaction_tv);
            expenseComment_tv = (TextView) convertView.findViewById(R.id.expenseComment_tv);
            expenseAmount_tv = (TextView) convertView.findViewById(R.id.expenseAmount_tv);
            expenseImageButton = (ImageButton) convertView.findViewById(R.id.expenseImageButton);
        }
    }
}

