package welcome.com.gmr_wallet.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import welcome.com.gmr_wallet.R;

import welcome.com.gmr_wallet.activity.AddIncomeActivity;
import welcome.com.gmr_wallet.activity.IncomeListActivity;
import welcome.com.gmr_wallet.db.DataBaseHandler;
import welcome.com.gmr_wallet.domain.IncomeListData;

/**
 * Created by home on 8/29/2016.
 */
public class MyCustomAdapterIncome extends BaseAdapter {

    ArrayList incomeList = new ArrayList();
    LayoutInflater myIncomeInflater;
    Context incomeContext;

    private IncomeListActivity incomeListActivity;

    public MyCustomAdapterIncome(Context context, ArrayList incomeList) {
        this.incomeList = incomeList;
        this.incomeContext = context;
        myIncomeInflater = LayoutInflater.from(context);

    }

    public void setIncomes(List<String> incomes) {
        if (incomes.size() > 0) {
            this.incomeList.clear();
            this.incomeList = incomeList;
        }
    }

    public void addItem(final String item) {
        incomeList.add(item);
    }
    @Override
    public int getCount() {
        return incomeList.size();
    }

    @Override
    public IncomeListData getItem(int position) {
        return (IncomeListData) incomeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        MyViewHolder holder = null;
        int type = getItemViewType(position);
        incomeList.get(position);
        if (convertView == null) {

            myIncomeInflater = (LayoutInflater) incomeContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = myIncomeInflater.inflate(R.layout.incomelist_textview, null);

            holder = new MyViewHolder(convertView);
            holder.incomeDT_tv = (TextView) convertView.findViewById(R.id.incomeDT_tv);
            holder.incomeTransaction_tv = (TextView) convertView.findViewById(R.id.incometransaction_tv);
            holder.incomeComment_tv = (TextView) convertView.findViewById(R.id.incomeComment_tv);
            holder.incomeAmount_tv = (TextView) convertView.findViewById(R.id.incomeAmount_tv);
            holder.incomeImageButton = (ImageButton) convertView.findViewById(R.id.incomeImageButton);


            convertView.setTag(holder);
        } else {
            holder = (MyViewHolder) convertView.getTag();
        }
        IncomeListData currentIncomeListData = getItem(position);
        holder.incomeDT_tv.setText(currentIncomeListData.getIncomeDT());
        holder.incomeTransaction_tv.setText(currentIncomeListData.getIncomeCategory());
        holder.incomeComment_tv.setText(currentIncomeListData.getIncomeComment());
        holder.incomeAmount_tv.setText(currentIncomeListData.getIncomeAmount());

        holder.incomeImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                final PopupMenu popup = new PopupMenu(incomeContext, v);
                popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {

                        if (item.getTitle().equals("update")) {
                            //TODO updte codegoes here
                            IncomeListData incomeListData = getItem(position);

                            Intent i = new Intent(incomeContext, AddIncomeActivity.class);
                            i.putExtra("eId", incomeListData.getID()+"");
                            i.putExtra("Date and Time",incomeListData.getIncomeDT());
                            i.putExtra("category", incomeListData.getIncomeCategory());
                            i.putExtra("comment", incomeListData.getIncomeComment());
                            i.putExtra("Amount", incomeListData.getIncomeAmount());

                            incomeContext.startActivity(i);

                        }
                        if (item.getTitle().equals("delete")) {
                            //delete code
                            IncomeListData item1 = getItem(position);
                            DataBaseHandler dataBaseHandler = new DataBaseHandler(incomeContext);
                            dataBaseHandler.deleteIncome(item1.getID());

                            Toast.makeText(incomeContext, "Income deleted successfully...", Toast.LENGTH_SHORT).show();
                            incomeList.clear();
                            List<IncomeListData> income = dataBaseHandler.getAllIncome();
                            incomeList.addAll(income);
                            notifyDataSetChanged();
                        }

                        return true;
                    }
                });

                popup.show();
            }


        });
        return convertView;
    }

    private class MyViewHolder {
        TextView incomeDT_tv;
        TextView incomeTransaction_tv;
        TextView incomeComment_tv;
        TextView incomeAmount_tv;
        ImageButton incomeImageButton;

        public MyViewHolder(View convertView) {
            incomeDT_tv = (TextView)convertView.findViewById(R.id.incomeDT);
            incomeTransaction_tv = (TextView) convertView.findViewById(R.id.incometransaction_tv);
            incomeComment_tv = (TextView) convertView.findViewById(R.id.incomeComment_tv);
            incomeAmount_tv = (TextView) convertView.findViewById(R.id.incomeAmount_tv);
            incomeImageButton = (ImageButton) convertView.findViewById(R.id.incomeImageButton);
        }
    }
}

