package welcome.com.gmr_wallet.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import welcome.com.gmr_wallet.R;
import welcome.com.gmr_wallet.activity.AddBudgetActivity;
import welcome.com.gmr_wallet.activity.BudgetListActivity;

import welcome.com.gmr_wallet.db.DataBaseHandler;
import welcome.com.gmr_wallet.domain.BudgetListData;

public class MyCustomAdapterBudget extends BaseAdapter {
    ArrayList budgetList = new ArrayList();
    LayoutInflater mInflater;
    Context budgetContext;


    private BudgetListActivity budgetListActivity;

    public MyCustomAdapterBudget(Context context, ArrayList budgetList) {
        this.budgetList = budgetList;
        this.budgetContext = context;
        mInflater = LayoutInflater.from(context);

    }


    public void setBudgets(List<String> budgets) {
        if (budgets.size() > 0) {
            this.budgetList.clear();
            this.budgetList = budgetList;
        }
    }

    public void addItem(final String item) {
        budgetList.add(item);
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return budgetList.size();
    }

    @Override
    public BudgetListData getItem(int position) {
        return (BudgetListData) budgetList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        MyViewHolder holder = null;
        int type = getItemViewType(position);
        budgetList.get(position);
        if (convertView == null) {

            mInflater = (LayoutInflater) budgetContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.budgetlistview_textview, null);

            holder = new MyViewHolder(convertView);
            holder.budgetDT_tv = (TextView)convertView.findViewById(R.id.budgetDT_tv);
            holder.budgetCategory_tv = (TextView) convertView.findViewById(R.id.budgetCategory);
            holder.budgetInterval_tv = (TextView) convertView.findViewById(R.id.budgetInterval_tv);
            holder.budgetName_tv = (TextView) convertView.findViewById(R.id.budgetName_tv);
            holder.budgetAmount_tv = (TextView) convertView.findViewById(R.id.budgetAmount_tv);
            holder.budgetImageButton = (ImageButton) convertView.findViewById(R.id.budgetImageButton);

            convertView.setTag(holder);
        } else {
            holder = (MyViewHolder) convertView.getTag();
        }
        BudgetListData currentBudgetListData = getItem(position);
        holder.budgetDT_tv.setText(currentBudgetListData.getBudgeDT());
        holder.budgetCategory_tv.setText(currentBudgetListData.getBudgetCategory());
        holder.budgetInterval_tv.setText(currentBudgetListData.getBudgetInterval());
        holder.budgetName_tv.setText(currentBudgetListData.getBudgetName());
        holder.budgetAmount_tv.setText(currentBudgetListData.getBudgetAmount());

        holder.budgetImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final PopupMenu popup = new PopupMenu(budgetContext, v);
                popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {

                        if (item.getTitle().equals("update")) {
                            // update codegoes here
                            BudgetListData budgetListData = getItem(position);

                            Intent i = new Intent(budgetContext, AddBudgetActivity.class);
                            i.putExtra("budgetId", budgetListData.getBudgetId(

                            ) + "");
                            i.putExtra("Date and Time",budgetListData.getBudgeDT());
                            i.putExtra("budgetCategory", budgetListData.getBudgetCategory());
                            i.putExtra("budgetInterval", budgetListData.getBudgetInterval());
                            i.putExtra("budgetName", budgetListData.getBudgetName());
                            i.putExtra("budgetAmount", budgetListData.getBudgetAmount());

                            budgetContext.startActivity(i);

                        }
                        if (item.getTitle().equals("delete")) {
                            //delete code
                            BudgetListData item1 = getItem(position);
                            DataBaseHandler dataBaseHandler = new DataBaseHandler(budgetContext);
                            dataBaseHandler.deleteBudget(item1.getBudgetId());

                            Toast.makeText(budgetContext, "Budget deleted successfully...", Toast.LENGTH_SHORT).show();
                            budgetList.clear();
                            List<BudgetListData> budget = dataBaseHandler.getAllBudget();
                            budgetList.addAll(budget);
                            notifyDataSetChanged();
                        }
                        return true;
                    }
                });
                popup.show();
            }
        });


        return convertView;
    }

    private class MyViewHolder {
        TextView budgetDT_tv;
        TextView budgetCategory_tv;
        TextView budgetInterval_tv;
        TextView budgetName_tv;
        TextView budgetAmount_tv;
        ImageButton budgetImageButton;

        public MyViewHolder(View convertView) {
            budgetDT_tv = (TextView) convertView.findViewById(R.id.budgetDT_tv);
            budgetCategory_tv = (TextView) convertView.findViewById(R.id.budgetCategory);
            budgetInterval_tv = (TextView) convertView.findViewById(R.id.budgetInterval_tv);
            budgetName_tv = (TextView) convertView.findViewById(R.id.budgetName_tv);
            budgetAmount_tv = (TextView) convertView.findViewById(R.id.budgetAmount_tv);
            budgetImageButton = (ImageButton) convertView.findViewById(R.id.budgetImageButton);
        }
    }

}



