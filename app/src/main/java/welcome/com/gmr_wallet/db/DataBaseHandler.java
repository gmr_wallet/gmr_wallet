package welcome.com.gmr_wallet.db;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import welcome.com.gmr_wallet.domain.BudgetListData;
import welcome.com.gmr_wallet.domain.ExpenseListData;
import welcome.com.gmr_wallet.domain.IncomeListData;

public class DataBaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "GMR-Wallet";
    private static final String KEY_BUDGET_DT = "budgetDT";
    private static final String TABLE_BUDGET = "addBudget";
    private static final String KEY_BUDGET_ID = "id";
    private static final String KEY_NAME = "budgetName";
    private static final String KEY_AMOUNT = "budgetAmount";
    private static final String KEY_CATEGORY = "budgetCategory";
    private static final String KEY_BUDGETINTERVAL = "budgetInterval";

    /*income */
    private static final String TABLE_INCOME = "addIncome";
    private static final String KEY_INCOME_ID = "incomeId";
    private static final String KEY_INCOME_DT = "incomeDT";
    private static final String KEY_INCOMETRANSCATION = "incomeTransaction";
    private static final String KEY_INCOMECOMMENT = "IncomeComment";
    private static final String KEYINCOME_AMOUNT = "IncomeAmount";

    /*expense */
    private static final String TABLE_EXPENSE = "addExpense";
    private static final String KEY_EXPENSE_ID = "expenseId";
    private static final String KEYEXPENSEDT = "expenseDT";
    private static final String KEY_EXPENSETRANSCATION = "expenseTransaction";
    private static final String KEYEXPENSE_COMMENT = "expenseComment";
    private static final String KEYEXPENSE_AMOUNT = "expenseAmount";


    public DataBaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_BUDGET_TABLE = " CREATE TABLE " + TABLE_BUDGET + " (" + KEY_BUDGET_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_NAME + " TEXT," + KEY_AMOUNT + " NUMBER," + KEY_CATEGORY + " TEXT," + KEY_BUDGETINTERVAL + " TEXT," + KEY_BUDGET_DT + " TEXT" + ")";
        db.execSQL(CREATE_BUDGET_TABLE);

        /*income table*/
        String CREATE_INCOME_TABLE = " CREATE TABLE " + TABLE_INCOME + " (" + KEY_INCOME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_INCOMECOMMENT+ " TEXT," + KEYINCOME_AMOUNT + " NUMBER," + KEY_INCOMETRANSCATION + " TEXT," + KEY_INCOME_DT + " TEXT" + ")";
        db.execSQL(CREATE_INCOME_TABLE);

        /*expense table*/
        String CREATE_EXPENSE_TABLE = " CREATE TABLE " + TABLE_EXPENSE + " (" + KEY_EXPENSE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEYEXPENSE_COMMENT + " TEXT," + KEYEXPENSE_AMOUNT + " NUMBER," + KEY_EXPENSETRANSCATION + " TEXT," + KEYEXPENSEDT + " TEXT" + ")";
        db.execSQL(CREATE_EXPENSE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BUDGET);
        onCreate(db);

        /* drop of income table*/
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INCOME);
        onCreate(db);
        /* drop of expense table*/
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXPENSE);
        onCreate(db);


    }

    public Cursor getDataBudget(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor bres = db.rawQuery("select * from addBudget where id=" + id + "", null);
        return bres;
    }

    /*dataincome readable*/
    public Cursor getDataIncome(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor ires = db.rawQuery("select * from addIncome where id=" + id + "", null);
        return ires;
    }

    /*dataexpense readable*/
    public Cursor getDataExpense(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor eres = db.rawQuery("select * from addExpense where id=" + id + "", null);
        return eres;

    }

    public boolean updateBudget(String id, String bName, String bAmount , String budgetCategory, String budgetInterval,String budgetDT) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("budgetDT",budgetDT);
        contentValues.put("budgetName", bName);
        contentValues.put("budgetAmount", bAmount);
        contentValues.put("budgetCategory", budgetCategory);
        contentValues.put("budgetInterval", budgetInterval);
        db.update(TABLE_BUDGET, contentValues, KEY_BUDGET_ID + "=" +id,null);
        return true;
    }



    /*updateexpense writable*/

        public void updateExpense(String id,String cmntExpense, String eAmount ,String expenseCategory,String expenseDT) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("expenseDT",expenseDT);
        contentValues.put("expenseComment", cmntExpense);
        contentValues.put("expenseAmount", eAmount);
        contentValues.put("expenseTransaction",expenseCategory);
        db.update(TABLE_EXPENSE, contentValues, KEY_EXPENSE_ID + "=" +id,null);



    }
    /*updateincome writable*/
    public void updateIncome(String id,String cmntIncome, String iAmount ,String incomeCategory,String incomeDT) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("incomeDT",incomeDT);
        contentValues.put("incomeComment", cmntIncome);
        contentValues.put("incomeAmount", iAmount);
        contentValues.put("incomeTransaction",incomeCategory);
        db.update(TABLE_INCOME, contentValues, KEY_INCOME_ID + "=" +id,null);
    }

    // Deleting single expense
    public void deleteExpense(int expenseId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_EXPENSE,  KEY_EXPENSE_ID + " = ?", new String[] { String.valueOf(expenseId) });
        db.close();
    }
    // Deleting single income
    public void deleteIncome(int incomeId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_INCOME,  KEY_INCOME_ID + " = ?", new String[] { String.valueOf(incomeId) });
        db.close();
    }

    // Deleting single budget

    public void deleteBudget(int budgetId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_BUDGET,  KEY_BUDGET_ID + " = ?", new String[] { String.valueOf(budgetId) });
        db.close();
    }



    public void insertBudgetInformation(String budgetName, String budgetAmount, String category, String budgetInterval,String budgetDT) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_BUDGET_DT,budgetDT);
        values.put(KEY_NAME, budgetName);
        values.put(KEY_AMOUNT, budgetAmount);
        values.put(KEY_CATEGORY, category);
        values.put(KEY_BUDGETINTERVAL, budgetInterval);
        db.insert(TABLE_BUDGET, null, values);
        db.close();
    }


    /*insertincomeinfomation*/
    public void insertIncomeInformation(String incomeComment, String incomeAmount, String incomeTranscation,String incomeDT) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_INCOME_DT,incomeDT);
        values.put(KEY_INCOMETRANSCATION, incomeTranscation);
        values.put(KEYINCOME_AMOUNT, incomeAmount);
        values.put(KEY_INCOMECOMMENT, incomeComment);
        db.insert(TABLE_INCOME, null, values);
        db.close();


    }

    /*insertexpenseinfomation*/
    public void insertExpenseInformation(String expenseComment, String expenseAmount, String expensetransaction, String expenseDT) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEYEXPENSEDT,expenseDT);
        values.put(KEY_EXPENSETRANSCATION, expensetransaction);
        values.put(KEYEXPENSE_AMOUNT, expenseAmount);
        values.put(KEYEXPENSE_COMMENT, expenseComment);
        db.insert(TABLE_EXPENSE, null, values);
        Log.i("Expense data:" , values.toString());
        db.close();


    }


    public List<BudgetListData> getAllBudget() {
        {
            List<BudgetListData> budgetLists = new ArrayList<BudgetListData>();

            String selectQuery = "SELECT * FROM " + TABLE_BUDGET + " ORDER BY datetime("+ KEY_BUDGET_DT + ") DESC";

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            cursor.moveToFirst();
            while (cursor.isAfterLast() == false) {
                BudgetListData budgetListData = extractBudgetsFromCursor(cursor);
                budgetLists.add(budgetListData);
                cursor.moveToNext();
            }

            cursor.close();
            db.close();
            return budgetLists;
        }
    }


    /*list of income */
    public List<IncomeListData> getAllIncome() {
        {
            List<IncomeListData> incomeLists = new ArrayList<IncomeListData>();

            String selectQuery = "SELECT * FROM " + TABLE_INCOME + " ORDER BY datetime("+ KEY_INCOME_DT + ") DESC";

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            cursor.moveToFirst();
            while (cursor.isAfterLast() == false) {
                IncomeListData incomeListData = extractIncomeFromCursor(cursor);
                incomeLists.add(incomeListData);
                cursor.moveToNext();
            }

            cursor.close();
            db.close();
            return incomeLists;
        }
    }

    /*list of expense*/
    @SuppressLint("LongLogTag")
    public List<ExpenseListData> getAllExpense() {
        {
            List<ExpenseListData> expenseLists = new ArrayList<ExpenseListData>();

            String selectQuery = "SELECT * FROM " + TABLE_EXPENSE + " ORDER BY datetime("+ KEYEXPENSEDT + ") DESC";

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            cursor.moveToFirst();
            while (cursor.isAfterLast() == false) {
                ExpenseListData expenseListData = extractExpenseFromCursor(cursor);
                expenseLists.add(expenseListData);
                cursor.moveToNext();
            Log.i("Getting all Expense data:" , ""+expenseListData.getID());
            }
            cursor.close();
            db.close();
            return expenseLists;
        }
    }

// write a method to get expense by id it returns expense

    private BudgetListData extractBudgetsFromCursor(Cursor cursor) {
        BudgetListData budgetListData = new BudgetListData();
        budgetListData.setBudgeDT(cursor.getString(5));
        budgetListData.setBudgetCategory(cursor.getString(3));
        budgetListData.setBudgetInterval(cursor.getString(4));
        budgetListData.setBudgetName(cursor.getString(1));
        budgetListData.setBudgetAmount(cursor.getString(2));
        budgetListData.setBudgetId(cursor.getInt(0));

        return budgetListData;
    }


    /*extractincome information*/
    private IncomeListData extractIncomeFromCursor(Cursor cursor) {
        IncomeListData incomeListData = new IncomeListData();
        incomeListData.setIncomeDT(cursor.getString(4));
        incomeListData.setIncomeCategory(cursor.getString(3));
        incomeListData.setIncomeComment(cursor.getString(1));
        incomeListData.setIncomeAmount(cursor.getString(2));
        incomeListData.setID(cursor.getInt(0));
        return incomeListData;
    }

    /*extractexpense information*/
    private ExpenseListData extractExpenseFromCursor(Cursor cursor) {
        ExpenseListData expenseListData = new ExpenseListData();
        expenseListData.setExpenseDT(cursor.getString(4));
        expenseListData.setExpenseCategory(cursor.getString(3));
        expenseListData.setExpenseComment(cursor.getString(1));
        expenseListData.setExpenseAmount(cursor.getString(2));
        expenseListData.setID(cursor.getInt(0));

        return expenseListData;

    }



}
